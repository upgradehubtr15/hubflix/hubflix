import React from 'react';
import ReactPlayer from 'react-player';
import './watch.css';

const watch = (props) => {
    return (
        <div className="watch">
            <ReactPlayer 
                url={`https://www.youtube.com/watch?v=${props.trailer}`} 
                playing 
                width='100%'
                height='100%'
            />
        </div>
    );
};

export default watch;