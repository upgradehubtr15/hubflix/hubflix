import React, { Component } from 'react';
import './signUp.css';
class signUp extends Component {
    state = {
        error : ""
    }


    userNameRef = React.createRef();
    nameRef = React.createRef();
    emailRef = React.createRef();
    passwordRef = React.createRef();
    password2Ref = React.createRef();
    creditCardRef = React.createRef();

    validateEmail(email)
    {
        var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
        console.log(re.test(email));
        return re.test(email);
    }

    check(datos) {
        console.log(datos);

        if(!this.validateEmail(datos.email))
        {
            this.setState({
                error : "Email invalido",
            });

            return false;
        }

        console.log(datos.password , datos.password2);
        if(datos.password !== datos.password2)
        {
            this.setState({
                error : "Las contraseñas no coinciden",
            });

            return false;
        }
    
        return true;
    }

    
    sendSignUp = (e) => {
        e.preventDefault();

        const datosRegistro ={
            userName: this.userNameRef.current.value,
            name: this.nameRef.current.value,
            email: this.emailRef.current.value,
            password: this.passwordRef.current.value,
            password2: this.password2Ref.current.value,
            creditCard: this.creditCardRef.current.value
        }
        //console.log(datosRegistro)
        //this.props.signUp(datosRegistro);
        if (this.check(datosRegistro)) {
    
        fetch('http://localhost:3000/NewUsuario', {
        method: 'POST',
        headers:{
            'Content-Type' : 'application/json',
            'Accept': 'application/json'
        },
        body:JSON.stringify({
                username: this.userNameRef.current.value,
                nombre: this.nameRef.current.value,
                email: this.emailRef.current.value,
                password: this.passwordRef.current.value,
                creditCard: this.creditCardRef.current.value
            })
        })
        .then(response =>{response.json()})
        .then(
            window.location.replace("./signin")
        )
        // .then((body)=>{
        //     console.log(body);
        //     if ( message.hasChildNodes())
        //     {
        //         while ( message.childNodes.length > 0 )
        //         {
        //             message.removeChild( message.lastChild );
        //         }
        //     }

        //     for(let i = 0;i<body.msg.length;i++)
        //     {
        //         let linea = document.createElement('li');
        //         linea.textContent=body.msg[i];
        //         message.appendChild(linea);
        //     }

        //     if(body.todoOk)
        //     {
        //         let linea = document.createElement('li');
        //         linea.textContent="El usuario ha sido creado exitosamente";
        //         message.appendChild(linea);

        //         this.userNameRef.current.value = "",
        //         this.nameRef.current.value ="",
        //         this.emailRef.current.value ="",
        //         this.passwordRef.current.value ="",
        //         this.password2Ref.current.value ="",
        //         this.creditCardRef.current.value =""
        //         }
        // })
        .catch(error => {
            console.error(error);
        });
        }
    }

    
    
    render() {
        //let boton = document.querySelector('#boton');
        //boton.addEventListener('click', validarDatos);

        return (
            <div className="signUp">
                <div className="signUpFormContainer">
                    <h1>Registro</h1>
                    <form className="signUpForm" id="signUp" onSubmit={this.sendSignUp}>
                        <input  ref={this.userNameRef} type="text" placeholder="nickName" required/>
                        <input  ref={this.nameRef} type="text" placeholder="nombre" required/>
                        <input  ref={this.emailRef} type="email" placeholder="email" required/>
                        <input  ref={this.passwordRef} type="password" placeholder="contrasenia" required pattern=".{6,8}" title="La contraseña debe tener entre 6 y 8 caracteres"/>
                        <input  ref={this.password2Ref} type="password" placeholder="contrasenia2" required/>
                        <input  ref={this.creditCardRef} type="text" placeholder="tarjetaDeCredito"/>
                        <button>SignUp</button>
                    </form>
                    <div>
                        <ul className="msg">
                            <p>{this.state.error}</p>
                        </ul>
                    </div>
                </div>
            </div>




        );
    }
}

export default signUp;