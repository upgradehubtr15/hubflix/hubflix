import React, { Component } from 'react';
import './search.css';


class Search extends Component {
    nombrePeliculaRef = React.createRef();
    genreRef = React.createRef();
    releaseDateRef = React.createRef();
    lateDateRef = React.createRef();
    actorRef = React.createRef();

    buscarPelicula = (e) => {
        e.preventDefault();

        const datosBusqueda ={
            nombre: this.nombrePeliculaRef.current.value
        }
        this.props.obtenerPelis(datosBusqueda);
    }

    busquedaAvanzadaPelicula = (e) => {
        e.preventDefault();

        const datosBusqueda ={
            genre: this.genreRef.current.value,
            releaseDate: this.releaseDateRef.current.value,
            lateDate: this.lateDateRef.current.value,
            actor: this.actorRef.current.value

        }
        this.props.busquedaAvanzada(datosBusqueda);
        //console.log(datosBusqueda);
    }

    showGenres= (key) => {
        const genre = this.props.genres[key];
        const {id, name} = genre;
        if(!id || !name) return null;
        return (
            <option key={id} value={id}>{name}</option>
        )
    }


    btnAdvancedSearch() {
        //console.log('tapped');
        setTimeout(function(){ document.getElementById("simpleSearch").style.display = "none"; }, 1);
        setTimeout(function(){ document.getElementById("advancedSearch").style.display = "block"; }, 1);
    }
    btnSimpleSearch() {
        //console.log('tapped');
        setTimeout(function(){ document.getElementById("simpleSearch").style.display = "block"; }, 1);
        setTimeout(function(){ document.getElementById("advancedSearch").style.display = "none"; }, 1);
        
    }
    

    
    render() {

        const genres = Object.keys(this.props.genres);
        return (
            <div className="searchComponent">
                <form id="simpleSearch" onSubmit={this.buscarPelicula}>
                    <input className="textInput" ref={this.nombrePeliculaRef} type="text" placeholder="Titulos, personas, generos"/>
                    <button>Buscar</button>
                    <span onClick={this.btnAdvancedSearch}>Busqueda Avanzada</span>
                </form>
                <form id="advancedSearch" onSubmit={this.busquedaAvanzadaPelicula}>
                    <select ref={this.genreRef}>
                        <option value="">Selecciona Categoria</option>
                        {genres.map(this.showGenres)}
                    </select>
                    Fecha de estreno
                    <input ref={this.releaseDateRef}  type="date" />
                    Estrenado antes de
                    <input ref={this.lateDateRef}  type="date" />

                    <input className="textInput" ref={this.actorRef} type="text" placeholder="Buscar por actor"/>
                    
                    {/*<input ref={this.actorRef} list="browsers" name="browser" />
                        <datalist id="browsers">
                            <option value="a"></option>
                            <option value="b"></option>
                            <option value="c"></option>
                        </datalist>*/}
                    <button>Buscar</button>
                    <span onClick={this.btnSimpleSearch}>Busqueda Simple</span>
                </form>

            </div>
        );
        
    }
}

export default Search;