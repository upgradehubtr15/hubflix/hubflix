import React from 'react';
import './movie.css';



const Movie = (props) => {

    const {title, id, backdrop_path} = props.movie;

    if (!backdrop_path) {
        return null
    }
    //console.log(title + backdrop_path);

    return (
        <div className="movie">
            <a href={`/movie/${id}`}>
                <img className="img-responsive" src={`https://image.tmdb.org/t/p/w500//${backdrop_path}`} alt={title}  />
                <p>{title}</p>
            </a>
        </div>
    );
};

export default Movie;


