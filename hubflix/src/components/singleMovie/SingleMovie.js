import React, { Component } from 'react';
import './singleMovie.css';
import mySvg from './thumbs-up.svg';

class SingleMovie extends Component {
    constructor(props) {
        super(props);
        this.sendVote = this.sendVote.bind(this);
        this.loadVote = this.loadVote.bind(this);
        this.loadComment = this.loadComment.bind(this);
        this.state = {
            votes : 0,
            comments : [{movieTitle : ""}]
        }
      }
    
    btnTapped() {
        //console.log('tapped');
        document.getElementById("overlay").style.opacity = "0";
        setTimeout(function(){ document.getElementById("overlay").style.display = "none"; }, 2000);
        document.getElementById("content").style.opacity = "0";
        setTimeout(function(){ document.getElementById("content").style.display = "none"; }, 2000);
    }
    
    sendVote() {
        var title = this.props.singleMovie.title;
        var vote = {
            title
        };
        //console.log(toDoElement);
        fetch("http://localhost:3000/vote",
        {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(vote)
        })
        .then((response) => {
            return response.json();
        })
        .then((myJson) => {
            //this.loadVote();
            this.loadVote();
        });
    };

    loadVote() {
        let totalVotes = 0
        fetch("http://localhost:3000/vote")
        .then((resp) => resp.json()) // Transform the data into json
        .then(data =>{
            {data.forEach(vote => { 
                if (vote.title === this.props.singleMovie.title) {
                    totalVotes +=1
                }   
                })}
            this.setState({
                votes : totalVotes
            })
        })
    };
      
    componentDidMount () {
        //console.log(this.props.singleMovie.title)
        this.loadVote();
        //cuando hace aqui el loadVote, this.props.singleMovie.title es null, porque no ha cargado, y me devuelve 0 totalVotes
    }


    commentContentRef = React.createRef();

    sendComment = (e) => {
        e.preventDefault();
        
        
        
        const datosComentario ={
            commentContent: this.commentContentRef.current.value,
            movieTitle: this.props.singleMovie.title,
        }
        
        console.log(datosComentario);
        //console.log(toDoElement);
        fetch("http://localhost:3000/comment",
        {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(datosComentario)
        })
        .then((response) => {
            return response.json();
        })
        .then((myJson) => {
            //this.loadVote();
            this.loadComment();
            document.getElementById("CommentInput").value = "";
        });
    }
    
    loadComment() {
        document.getElementById("Comment").style.display = "block";
        document.getElementById("showComments").style.display = "block";
        
        let movieComments = []
        fetch("http://localhost:3000/comment")
        .then((resp) => resp.json()) // Transform the data into json
        .then(data =>{
            {data.forEach(comment => { 
                if (comment.movieTitle === this.props.singleMovie.title) {
                    movieComments.push(comment);
                }   
                })}
            this.setState({
                comments : movieComments
            })
        })
    };


    
    render() {
        const genres = [];
        if (this.props.singleMovie.genres !== undefined) {
            this.props.singleMovie.genres.map(genre=>{
                genres.push(genre);
                return null
            })
        }
        let director = ""
        if (this.props.crew !== undefined) {
            director = this.props.crew[0].name;
            //console.log(this.props.crew[0].name)
        }
        //let ver = <a href={'./watch/' + this.props.trailer}>Ver</a>;

        let comentarios = []
        if (this.props.comments !== undefined) {
            this.props.comments.map(comment => 
            comentarios.push(comment.movieTitle)
            )
        }




        return (
            <div className="singleMovieContainer">
                <div className="content" id="content">
                    <img className="img-responsive" src={`https://image.tmdb.org/t/p/w500//${this.props.singleMovie.poster_path}`} alt={this.props.singleMovie.title}  />
                    <div className="movieDetails">
                        <p className="movieTitle">{this.props.singleMovie.title}</p>
                        <div className="subtitle">
                            <span className="score">{this.props.singleMovie.vote_average}</span>
                            <span className="duration">{this.props.singleMovie.runtime}min</span>
                            <span className="quality">HD</span>
                        </div>
                        <p className="sinopsis">{this.props.singleMovie.overview}</p>
                        <p className="movieDirector"> Director: {director} </p>
                        <div className="genreInfo">
                            {genres.map(genre => <p className="movieGenre"> {genre.name} </p>)}
                        </div> <br/>
                        <div className="buttonsContainer">
                            <a href={'./watch/' + this.props.trailer} className="watchMovie">Ver</a> 
                            <button onClick={this.sendVote} className="vote">
                                <img className="likeImg" src={mySvg} alt="like"/>  {this.state.votes}
                            </button>                        
                            <div className="commentContainer">
                                <button onClick={this.loadComment}>
                                    Comentarios
                                </button>
                                <form className="CommentForm" id="Comment" onSubmit={this.sendComment}>
                                    <input className="CommentInput" id="CommentInput" ref={this.commentContentRef} type="text" placeholder="Comment"/>
                                    <button>Comenta!</button>
                                </form>
                                <div id="showComments">
                                    {this.state.comments.map(comment => 
                                        <p > {comment.commentContent} </p>
                                        )}
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        );
    }
}

export default SingleMovie;