import React, { Component } from 'react';
import Search from '../search/Search';
import Movies from '../movies/Movies';
import ReactPlayer from 'react-player';
import './inicio.css';

class Inicio extends Component {

    state = {
        searchMovies : [],
        genres : [],
        searchActors : [],
    }
    
    componentDidMount() {
        this.obtenerCategorias();
        document.querySelector(".react-player__preview").addEventListener("click", this.myFunction);

    }
    
    obtenerCategorias= async () => {
        let url = `https://api.themoviedb.org/3/genre/movie/list?api_key=0e9bb2772155ea9ea2b9909c732b76c8&language=en-US`;
        await fetch(url)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(genre =>{
            this.setState({
                genres: genre.genres
            }) 
        })
    }
    
    obtenerPeliculas = async (busqueda) => {
        const token = "0e9bb2772155ea9ea2b9909c732b76c8"
        let url = `https://api.themoviedb.org/3/search/multi?api_key=${token}&language=es-ES&query=${busqueda.nombre}&page=1&sort_by=popularity.desc`
        let arrayMovies = []
        
        await fetch(url)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(movies =>{
            movies.results.forEach(movie => {
                if (movie.backdrop_path != null) {
                    arrayMovies.push(movie)
                }
            });
            
            this.setState({
                searchMovies: arrayMovies,
                trailerInicio: arrayMovies
            }) 
        })
    }


    busquedaAvanzada = async (busqueda) => {
        let actor = busqueda.actor
        let genre = `&with_genres=${busqueda.genre}` 
        let actorId = ""
        let releaseDate = `&primary_release_date.gte=${busqueda.releaseDate}` 
        let lateDate = `&primary_release_date.lte=${busqueda.lateDate}`
        let token = "0e9bb2772155ea9ea2b9909c732b76c8"
        if (actor !== "") {
            let url = `http://api.tmdb.org/3/search/person?api_key=0e9bb2772155ea9ea2b9909c732b76c8&query=${actor}`
            let arrayActors = []
            await fetch(url)
            .then(respuesta => {
                return respuesta.json();
            })
            .then(actors =>{
                actors.results.forEach(actor => {
                    arrayActors.push(actor)
                });
                if (arrayActors.length !== 0) {
                    actorId = arrayActors[0].id;
                }
                this.setState({
                    searchActors: arrayActors
                }) 
            })
        }
        let cast = `&with_cast=${actorId}`
        let url = `https://api.themoviedb.org/3/discover/movie?api_key=${token}&language=es-ES&sort_by=popularity.desc&include_adult=false&include_video=false&page=1${genre}${releaseDate}${lateDate}${cast}`
        let arrayMovies = []
        await fetch(url)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(movies =>{
            while (arrayMovies.length < 7) {
                movies.results.forEach(movie => {
                    if (movie.backdrop_path != null) {
                        arrayMovies.push(movie)
                    }
                });

            }
            this.setState({
                searchMovies: arrayMovies
            }) 
        })
        document.getElementById("searchTitle").style.display = "block";

    }

    myFunction (){
        document.getElementById("movieInicioNowIn").style.display = "none";
        document.getElementById("movieInicioTitle").classList.add("InicioTitleNewClass");
        
    }
    
    render() {

        let expiresdate = new Date(2020, 1, 1, 0, 0);
        let cookievalue = "Hubflix rules!";
        document.cookie = "cookflix=" + encodeURIComponent( cookievalue ) + "; expires=" + expiresdate.toUTCString() + "; path=/; domain=hubflix.com;secure";
        return (
            <div className="inicioComponent">
                <Search
                    obtenerPelis={this.obtenerPeliculas}
                    genres = {this.state.genres}
                    busquedaAvanzada={this.busquedaAvanzada}
                />
                <div className="trailerInicio">
                    <div >
                        <p id="movieInicioTitle" className="titleMovieInicio">{this.props.movieInicio.title}</p>
                        <p id="movieInicioNowIn" className="onHubFlix">Ahora en <span className="blue">HUB</span><span className="red">FLIX</span></p>
                    </div>
                    {/* <a className="watchNow" href={'/movie/watch/' + this.props.trailerInicio} > Reproducir</a>     */}
                    {/* <img className="imgInicio" src={`https://image.tmdb.org/t/p/original//${this.props.movieInicio.backdrop_path}`} alt={"title"}  /> */}
                    <ReactPlayer 
                        url={`https://www.youtube.com/watch?v=${this.props.trailerInicio}`} 
                        light={`https://image.tmdb.org/t/p/original//${this.props.movieInicio.backdrop_path}`}
                        width='100%'
                        height='100%'
                        playing
                    />
                    {console.log(ReactPlayer)}
                </div>

                <div className="searchedMovies">
                <h1 id="searchTitle">Busqueda</h1>
                    <Movies
                        movies={this.state.searchMovies}
                    />
                </div>
                <div className="popularMovies">
                    <h1>Acción</h1>
                    <Movies
                        movies={this.props.movies}
                    />
                </div>
                <div>
                    <h1 className="row carouselTitle">{this.props.genre}</h1>
                    <Movies
                        movies={this.props.byGenre}
                        />
                </div>
            </div>
        );
    }
}

export default Inicio;


