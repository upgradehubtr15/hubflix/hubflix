import React from 'react';
import './header.css';
import myImg from './hubflix.png';



const Header = () => {
    let logOut = () => {
        localStorage.removeItem("jwtToken");
        localStorage.removeItem("username");
        window.location.replace("./")

    }   

    let name = localStorage.getItem('username');
    let iniciarSesion = "";
    let LogOutText = "LogOut";
    if (!name) {
        name = "";
        LogOutText = "";
        iniciarSesion = "Iniciar Sesión"

    }
    return (
        <div className="header">
            <div>
                <ul>
                    <li className="logo">
                        <a href="/inicio"><img className="logoImg" src={myImg} alt="logo"/></a>
                    </li>
                    <div className="sesion">
                    <li>
                        <a href="/signin">{iniciarSesion}</a>
                    </li>
                    <li>
                        <p>{name}</p>
                    </li>
                    <p onClick={logOut}>
                        {LogOutText}
                    </p>
                   </div>
                </ul>
            </div>

        </div>
    );
};

export default Header;