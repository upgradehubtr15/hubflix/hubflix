import React, { Component } from 'react';
import './signIn.css';


class signIn extends Component {
    usernameRef = React.createRef();
    passwordRef = React.createRef();


    sendSignIn = (e) => {
        e.preventDefault();

        const datosRegistro ={
            username: this.usernameRef.current.value,
            password: this.passwordRef.current.value,
        }
        //console.log(datosRegistro)
        //this.props.signUp(datosRegistro);
    
        fetch('http://localhost:3000/login', {
        method: 'POST',
        headers:{
            'Content-Type' : 'application/json',
            'Accept': 'application/json'
        },
        body:JSON.stringify(
                datosRegistro
            )
        })
        .then(response =>{return response.json()})
        .then( body => {
            console.log(body)
            localStorage.setItem('jwtToken', body.token);
            localStorage.setItem('username', body.username);
            window.location.replace("./inicio")

        })
        .catch(error => {
            console.error(error);
        });
    }


    render() {
        return (
            <div className="signIn">
                <div className="signInFormContainer">
                    <h1>Iniciar sesión</h1>
                    <form className="signInForm" id="signIn" onSubmit={this.sendSignIn}>
                    {/* <form className="signInForm" id="signIn" action="http://localhost:3000/login" method="post"> */}
                        <input className="emailSignInInput" ref={this.usernameRef} type="text" placeholder="User"/>
                        <input className="passwordSignInInput" ref={this.passwordRef} type="password" placeholder="Password"/>
                        <button>SignIn</button>
                    </form>
                    <p className="notSubscribed">¿Todavía sin HubFlix? <span> <a href="/"> Suscríbete ya.</a></span></p>
                </div>
            </div>
        );
    }
}

export default signIn;