import React, { Component } from 'react';
import Movie from '../movie/Movie';
import './movies.css';
import Slider from "react-slick";


export default class Movies extends Component {
    render() {
      const settings = {
        className: "center",
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
          {
            breakpoint: 1500,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4
            }
          },
          {
            breakpoint: 1250,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 1000,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]

      };
        return (
            <div>
                <Slider {...settings}>
                    {this.props.movies.map(movie => (
                        <Movie
                        key = {movie.id}
                        movie= {movie} />
                        ))}
                </Slider>
            </div>
        );
    }
}


