import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Inicio from './components/inicio/Inicio';
import Header from './components/header/Header';
import Movies from './components/movies/Movies';
import SingleMovie from './components/singleMovie/SingleMovie';
import Profile from './components/profile/Profile';
import SignUp from './components/signUp/signUp';
import SignIn from './components/signIn/signIn';
import Watch from './components/watch/watch';
import './router.css';

class Router extends Component {

    state = {
        movies : [],
        singleMovie : [],
        trailer : [],
        byGenre : [],
        genre : "Documentales",
        movieInicio : {},
        trailerInicio: ""
    }

    componentDidMount() {
        this.getMovies();
        this.getByGenre();
    }

    getMovies = () => {
        let url = 'https://api.themoviedb.org/3/discover/movie?api_key=0e9bb2772155ea9ea2b9909c732b76c8&language=es-ES&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&vote_average.gte=6&with_genres=28';
        
        fetch(url)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(movies =>{
            this.getTrailer(movies.results[0].id)
            this.setState({
                movieInicio : movies.results[0],
                movies : movies.results
            })
        })
    }   
    getByGenre = () => {
        let url = 'https://api.themoviedb.org/3/discover/movie?api_key=0e9bb2772155ea9ea2b9909c732b76c8&language=es-ES&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&vote_average.gte=6&with_genres=99';
        
        fetch(url)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(movies =>{
            this.setState({
                byGenre : movies.results,
            })
        })
    }   

    getSingleMovie = (movieId) => {
        let url = `https://api.themoviedb.org/3/movie/${movieId}?api_key=0e9bb2772155ea9ea2b9909c732b76c8&language=es-ES`;
        
        fetch(url)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(movie =>{
            this.setState({
                singleMovie : movie
            })
        })
    }   

    getTrailer = (movieId) => {
        let urlTrailer = `https://api.themoviedb.org/3/movie/${movieId}/videos?api_key=0e9bb2772155ea9ea2b9909c732b76c8&language=es-ES`;
        fetch(urlTrailer)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(trailer =>{
            if (trailer.results.length !== 0 ) {
                this.setState({
                    trailerInicio : trailer.results[0].key,
                    trailer : trailer.results[0].key
                })
            }
        })
    }  

    signIn = (signInData) => {
        console.log(signInData);
    }   


    comment = (commentData) => {
        console.log(commentData);
    }   

    getCrew = (movieId) => {
        let urlCrew = `https://api.themoviedb.org/3/movie/${movieId}/credits?api_key=0e9bb2772155ea9ea2b9909c732b76c8`
        fetch(urlCrew)
        .then(respuesta => {
            return respuesta.json();
        })
        .then(crew =>{
            this.setState({
                crew : crew.crew
            })
        })
    }  

    render() {
        const {movies} = this.state
        
        if (!movies || movies.length === 0) {
            return (<p>Loading...</p>)
        }
        
        return (
           <BrowserRouter>
                <div className="container-fluid">
                <Header />
                    <Switch>
                    <Route exact path="/" render={() => (
                            <SignUp/>    
                        ) } />
                        <Route exact path="/signin" render={() => (
                            <SignIn
                            signIn={this.signIn}
                            />    
                        ) } />

                        <Route exact path="/inicio" render={() => (
                            <Inicio
                            movies={this.state.movies}
                            genre={this.state.genre}
                            byGenre={this.state.byGenre}
                            trailerInicio={this.state.trailerInicio}
                            movieInicio = {this.state.movieInicio}
                            />    
                            ) } 
                        />
                        <Route exact path="/movies" render={() => (
                            <Movies
                            movies={this.state.movies}
                            />    
                        ) } />
                        <Route exact path="/movie/:movieId" render={(props) => {
                            const movieId = parseInt(props.match.params.movieId);
                            if (this.state.singleMovie.id !==  movieId) {
                                this.getSingleMovie(movieId);
                                this.getTrailer(movieId);
                                this.getCrew(movieId);
                            }
                            return (
                                <SingleMovie
                                singleMovie={this.state.singleMovie}
                                trailer={this.state.trailer}
                                crew={this.state.crew}

                                />
                                )
                            } } />
                        <Route exact path="/movie/watch/:trailer" render={(props) => {
                            const trailer = (props.match.params.trailer);
                            return (
                                <Watch
                                    trailer={trailer}
                                    />
                            )
                        } } />
                        <Route exact path="/profile" render={() => (
                            <Profile
                            />    
                            ) } 
                        />

                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

export default Router;