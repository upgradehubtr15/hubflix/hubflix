mySQL

Para crear el contenedor hay dos opciones:
$ docker run -d -p 33060:3306 -v /c/Users/rober/Desktop/ProyectoFinal/hubflix/mySQL=/var/lib/mysql --name hubflix-db -e MYSQL_ROOT_PASSWORD=upgrade mysql

La ruta "/c/Users/rober/Desktop/ProyectoFinal" es mi ruta al proyecto, cuando levanteis el contenedor en vuestra m�quina con la base de datos ponedle el 
volumen en la ruta que necesiteis, pero sin cambiar la ruta interna que es donde por defecto se guardan las bases de datos mySQL.

$ docker run -d -p 33060:3306 -v PWD=/var/lib/mysql --name hubflix-db -e MYSQL_ROOT_PASSWORD=upgrade mysql
Con esta opci�n deberiamos ir al directorio donde tengamos /hubflix/mySQL que esla parte que tenemos en git y ejecutamos el comando anterior.

Hay que recordar que la base de datos se escucha por el puerto 3306.


Para acceder a la base de datos como lo hacemos habitualmente fuera el comando es:
$ docker exec -it hubflix-db mysql -p

En cuanto se ponga el comando nos pedira la contrase�a, cuando creamos el contenedor le pusimos la contrase�a 'upgrade' as� que eso es lo que hay que poner.

Y en cuanto cargue ya estamos dentro de la base de datos.
